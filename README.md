# README

## Project structure

```
.
├── dist                    # Compiled files
├── src                     # Source files
├── LICENSE.md
└── README.md
```

## Build project

### Set up the environment

#### Install Node.js and its modules

1. Install Node.js, find the installation instructions at <https://nodejs.org/en/download/>.
2. cd to the project directory and install the project dependencies: `npm install`.
3. Install gulp globally: `npm install -g gulp`.
4. Install gulp in the project directory: `npm install gulp`.

#### Install SASS

1. Install Ruby, find the installation instructions at <https://www.ruby-lang.org/en/documentation/installation/>.
2. Install SASS `sudo su -c "gem install sass"`.

### Dev build

Run `gulp default` to compile the assets once or `gulp watch` to compile on save during the development.

### Prod build

To compile for the production run `gulp default --env production`. This will uglify/minify the scripts and stylesheets.

## Assets converters

### Favicon

Generate favicons for different platforms <http://realfavicongenerator.net/>. The source favicon image is in `src/original/favicon.png`.

### Logo

- Convert PNG logo image to SVG <http://picsvg.com/>
- Optimize GIF animation <http://ezgif.com/optimize>

## Acknowledgements

### Icons

- <https://www.iconfinder.com/iconsets/faticons-2> License: Free for commercial use
- <https://www.iconfinder.com/iconsets/e-commerce-icon-set> License: Free for commercial use
- <https://www.iconfinder.com/iconsets/socialcons> License: Free for commercial use
- <https://www.iconfinder.com/iconsets/picons-social> License: Free for commercial use
- <https://www.iconfinder.com/iconsets/flags_gosquared> License: Free for commercial use
