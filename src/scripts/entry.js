window.onload = function() {
  'use strict';

  /**
   * Navbar trigger for small screens
   */
  $('.menu-list-trigger').on('click', function() {
    $('.pure-menu-list').toggleClass('menu-list-slide-in');
  });

  /**
   * Contact us form
   */
  $('.js-contactus-submit').on('click', function() {
    var $form = $('.contact-form'),
      $heading = $('.js-contactus-tagline'),
      $success = $('.js-contactus-tagline-success');
    $form.addClass('transparent');
    setTimeout(function() {
      $form.addClass('hidden');
    }, 2000);
    $heading.addClass('hidden');
    $success.removeClass('transparent hidden');
  });

  /**
   * Masonry for project images grid
   */
  $('.project-grid').masonry({
    itemSelector: '.project-item',
    percentPosition: true
  });

  /**
   *
   * This calls sends an email to one recipient.
   *
   */
  process.env.MJ_APIKEY_PUBLIC = '40b0e16dd98b45a790314cd501dde235';
  process.env.MJ_APIKEY_PRIVATE = 'e78d3635d4b6d5f9394cb80c39599b19';
  var mailjet = require('node-mailjet')
    .connect(process.env.MJ_APIKEY_PUBLIC, process.env.MJ_APIKEY_PRIVATE);
  var request = mailjet
    .post("send")
    .request({
      "FromEmail": "savelyenne@gmail.com",
      "FromName": "Mailjet Pilot",
      "Subject": "Your email flight plan!",
      "Text-part": "Dear passenger, welcome to Mailjet! May the delivery force be with you!",
      "Html-part": "<h3>Dear passenger, welcome to Mailjet!</h3><br />May the delivery force be with you!",
      "Recipients": [{
        "Email": "savelyenne@gmail.com"
      }]
    });
  request
    .then(function(result) {
      console.log(result.body);
    })
    .catch(function(err) {
      console.log(err.statusCode);
    });
};
