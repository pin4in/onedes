/*!
 * gulp
 * $ npm install gulp-environments gulp-ruby-sass gulp-jade gulp-sourcemaps gulp-autoprefixer gulp-cssnano gulp-jshint gulp-concat gulp-uglify gulp-imagemin gulp-notify gulp-rename gulp-livereload gulp-cache del --save-dev
 */
'use strict';
// Load plugins
var gulp = require('gulp'),
  imagemin = require('gulp-imagemin'),
  rename = require('gulp-rename'),
  concat = require('gulp-concat'),
  notify = require('gulp-notify'),
  cache = require('gulp-cache'),
  livereload = require('gulp-livereload'),
  del = require('del'),
  // htmlmin = require('gulp-htmlmin'),
  sourcemaps = require('gulp-sourcemaps'),
  //Markup
  jade = require('gulp-jade'),
  //Styles
  sass = require('gulp-ruby-sass'),
  autoprefixer = require('gulp-autoprefixer'),
  cssnano = require('gulp-cssnano'),
  //Scripts
  jshint = require('gulp-jshint'),
  uglify = require('gulp-uglify'),
  //Environment
  env = require('gulp-environments'),
  dev = env.development,
  prod = env.production;

/**
 * MARKUP
 */
gulp.task('markup', function() {
  var YOUR_LOCALS = {};
  return gulp.src(['src/markup/**/*.jade', '!src/markup/partials/*.jade'])
    .pipe(jade({
      locals: YOUR_LOCALS
    }))
    .pipe(gulp.dest('dist'))
    .pipe(dev(notify({
      message: 'Markup task complete'
    })));
});

/**
 * STYLES
 */
gulp.task('styles', function() {
  return sass('src/styles/*.scss', {
      style: 'expanded'
    })
    .pipe(dev(sourcemaps.init()))
    .pipe(autoprefixer('> 1%'))
    .pipe(gulp.dest('dist/styles'))
    .pipe(prod(cssnano()))
    .pipe(dev(sourcemaps.write()))
    .pipe(gulp.dest('dist/styles'))
    .pipe(notify({
      message: 'Styles task complete'
    }));
});

/**
 * SCRIPTS
 */
gulp.task('scripts', function() {
  return gulp.src('src/scripts/**/*.js')
    .pipe(jshint('.jshintrc'))
    .pipe(jshint.reporter('default'))
    .pipe(dev(sourcemaps.init()))
    .pipe(concat('main.js'))
    .pipe(prod(uglify()))
    .pipe(dev(sourcemaps.write()))
    .pipe(gulp.dest('dist/scripts'))
    .pipe(notify({
      message: 'Scripts task complete'
    }));
});

/**
 * IMAGES
 */
gulp.task('images', function() {
  return gulp.src('src/images/**/*')
    .pipe(cache(imagemin({
      optimizationLevel: 3,
      progressive: true,
      interlaced: true,
      multipass: true
    })))
    .pipe(gulp.dest('dist/images'))
    .pipe(notify({
      message: 'Images task complete'
    }));
});

/**
 * COPY FAVICONS
 */
gulp.task('copyfavicons', function() {
  return gulp.src('src/favicons/**/*.*')
    .pipe(gulp.dest('dist/favicons'))
    .pipe(notify({
      message: 'Copy favicons task complete'
    }));
});

/**
 * COPY FONTS
 */
gulp.task('copyfonts', function() {
  return gulp.src('src/fonts/**/*.*')
    .pipe(gulp.dest('dist/fonts'))
    .pipe(notify({
      message: 'Copy fonts task complete'
    }));
});

/**
 * Clean destination dirs
 */
gulp.task('clean', function() {
  return del(['dist/**/*']);
});

/**
 * Default task
 */
gulp.task('default', ['clean'], function() {
  gulp.start('markup', 'styles', 'scripts', 'images', 'copyfavicons', 'copyfonts');
});

/**
 * Watch
 */
gulp.task('watch', function() {
  // Watch .html files
  gulp.watch('src/**/*.jade', ['markup']);
  // Watch .scss files
  gulp.watch('src/styles/**/*.scss', ['styles']);
  // Watch .js files
  gulp.watch('src/scripts/**/*.js', ['scripts']);
  // Watch image files
  gulp.watch('src/images/**/*', ['images']);
  //Copy favicons dirs
  gulp.watch('src/favicons/**/*.*', ['copyfavicons']);
  //Copy fonts dirs
  gulp.watch('src/fonts/**/*.*', ['copyfonts']);
  // Create LiveReload server
  livereload.listen();
  // Watch any files in dist/, reload on change
  gulp.watch(['dist/**']).on('change', livereload.changed);
});
